import numpy as np
import pandas as pd
from random import randint
import matplotlib.pyplot as plt
from numpy.linalg import inv
from numpy.linalg import pinv


def f(chosen_center, point, power):
    a = (chosen_center[0] - point[0]) ** power + (chosen_center[1] - point[1]) ** power
    return a


def p_norm_gradient(power, point, chosen_center, price, x):
    a = price * ((chosen_center[x] - point[x]) * ((chosen_center[x] - point[x]) ** (power - 2))
                  * (f(chosen_center, point, power) ** ((1 / power) - 1)))
    return a
def dev(power, point, chosen_center, price, x, xx):
    learning_rate = 0.01
    chosen_center_x,chosen_center_y = chosen_center
    if xx == 0:
        chosen_center_x = chosen_center_x + 0.1
    else:
        chosen_center_y = chosen_center_y + 0.1
    chosen_center1 = [chosen_center_x, chosen_center_y]
    a = (p_norm_gradient(power, point, chosen_center1,price, x)
         - p_norm_gradient(power, point, chosen_center,price, x)) / (learning_rate)
    return a

def p_norm_hessian_same(power, point, chosen_center, price, x):
    a = price * ((power - 1) * (chosen_center[x] - point[x]) ** (power - 2) * (f(chosen_center, point, power)) ** ((1 / power) -1)
    + (chosen_center[x] - point[x]) ** (2 * power - 2) * (f(chosen_center, point, power)) ** ((1 / power) - 2) * (1 - power))
    return a


def p_norm_hessian_diff(power, point, chosen_center, price):
    a = price * ((chosen_center[0] - point[0]) ** (power - 1) * (chosen_center[1] - point[1]) ** (power - 1) \
        * (f(chosen_center, point, power)) ** ((1 / power) - 2))
    return a


def p_norm_loss(power, point, chosen_center, price):
    a =  price * (((chosen_center - point) ** (power)) ** (1 / power))
    return a


def fit(A, means, power, antena_power, antena, k, point_price, antena_price):
    n = len(A)
    Gx = np.zeros([n, k])
    Gy = np.zeros([n, k])
    Hxx = np.zeros([n, k])
    Hyy = np.zeros([n, k])
    Hyx = np.zeros([n, k])
    Hxy = np.zeros([n, k])
    for i in range(n):
        chosen_center_index = nearest(A[i, :], means, k, power)
        Gx[i, chosen_center_index] = p_norm_gradient(power, A[i, :], means[chosen_center_index, :], point_price, 0)
        Gy[i, chosen_center_index] = p_norm_gradient(power, A[i, :], means[chosen_center_index, :], point_price, 1)
        Hxx[i, chosen_center_index] = dev(power, A[i, :], means[chosen_center_index, :], point_price, 0, 0)
        Hyy[i, chosen_center_index] = dev(power, A[i, :], means[chosen_center_index, :], point_price, 1, 1)
        Hxy[i, chosen_center_index] = dev(power, A[i, :], means[chosen_center_index, :], point_price, 0, 1)
        Hyx[i, chosen_center_index] = dev(power, A[i, :], means[chosen_center_index, :], point_price, 1, 0)
    Gx_sum = Gx.sum(axis=0)
    Gy_sum = Gy.sum(axis=0)
    Hxx_sum = Hxx.sum(axis=0)
    Hyy_sum = Hyy.sum(axis=0)
    Hxy_sum = Hxy.sum(axis=0)
    Hyx_sum = Hyx.sum(axis=0)
    for i in range(k):
        Gx_sum[i] += 1000 * p_norm_gradient(antena_power, antena[i, :], means[i, :], antena_price, 0)
        Gy_sum[i] += 1000 * p_norm_gradient(antena_power, antena[i, :], means[i, :], antena_price, 1)
        Hxx_sum[i] += 1000 * dev(antena_power, antena[i, :], means[i, :], antena_price, 0, 0)
        Hyy_sum[i] += 1000 * dev(antena_power, antena[i, :], means[i, :], antena_price, 1, 1)
        Hxy_sum[i] += 1000 * dev(antena_power, antena[i, :], means[i, :], antena_price, 0, 1)
        Hyx_sum[i] += 1000 * dev(antena_power, antena[i, :], means[i, :], antena_price, 1, 0)

        # Hyx_sum[i] = Hyx_sum[i] + p_norm_hessian_same(antena_power, antena[i, 0], means[i, 0], antena_price)
    # Hx_avg[Hx_avg == 0] = 1
    # Hy_avg[Hy_avg == 0] = 1

    vec = []
    vec1 = []
    for i in range(k):
        grad = np.array([Gx_sum[i], Gy_sum[i]])
        # hess = np.matrix([[float(Hxx_sum[i]), float(Hxy_sum[i])], [float(Hyx_sum[i]), float(Hyy_sum[i])]])
        # hess_minus_1 = inv(hess)
        # total = np.dot(hess_minus_1, grad)
        # vec.append(total.tolist()[0])
        vec1.append(grad)
    return np.array(vec1)


def loss_point(A, means, power, k, point_price):
    n = len(A)
    B = np.zeros([n,k,D])
    B1 = np.zeros([n, k, D])
    for i in range(n):
        chosen_center_index = nearest(A[i, :], means)
        B[i,chosen_center_index,:] = p_norm_loss(power, A[i,:], means[chosen_center_index,:], point_price)
        B1[i, nearest(A[i, :], means), :] = 1
    B_sum = B.sum(axis=0)
    B1_sum = B1.sum(axis=0)
    B1_sum = np.where(B1_sum == 0, 1, B1_sum)
    # return (B_sum / B1_sum).sum()
    return B_sum.sum()


def loss_antena(antena, means, antena_power, k, antena_price):
    sum = 0
    for i in range(k):
        sum = sum + p_norm_loss(antena_power, antena[i], means[i], antena_price)
    # return (sum / k).sum()
    return sum.sum()


def nearest(a, means, k, power):
    vec = []
    a = a.reshape(1,2)
    for i in range(k):
        vec.append(f(means[i, :], a[0], power) ** (1 / power))
    vec = np.array(vec)
    return vec.argmin()


def df_maker(data, str, binary):
    df = pd.DataFrame(data)
    df.columns = ['X', 'Y']
    df['type'] = str
    df['counter'] = 2
    return df


def df_maker_center(data,str):
    df = pd.DataFrame(data)
    df.columns = ['X', 'Y','counter']
    df['type'] = str
    return df

if __name__ == '__main__':
    N = 300
    D = 2
    k = 3
    power = float(2)
    antena_power = float(2)
    point_price = 1
    antena_price = 100
    points = [[0, 0], [2, 2], [19, 5], [-11, -5],
              [-10, 44], [13, 10]]
    antena = np.array([[5, 5], [-10, 15], [-4, 0]])
    X = 5 * np.random.randn(N,D) + np.array([11, 23])
    for point in points:
        generated_point = 5 * np.random.randn(N,D) + np.array(point)
        X = np.concatenate((X,generated_point))
    data = X
    np.random.shuffle(data)
    n = len(data)
    nTrain = int(0.6*n)
    dataTrain = data[:nTrain,:]
    dataTest = data[nTrain:,:]
    kMean = 12*np.random.randn(k, D)
    center1 = []
    center2 = []
    center3 = []
    # for j in range(10):
    #     kMean = kMean1
    for i in range(100):
        vec1 = fit(dataTrain, kMean, power, antena_power, antena, k, point_price, 1)
        print(vec1)
        print(kMean)
        kMean = kMean - vec1
        print(kMean)
        print(1)
    center1.append([kMean[0][0], kMean[0][1], 1])
    center2.append([kMean[1][0], kMean[1][1], 1])
    center3.append([kMean[2][0], kMean[2][1], 1])

    # lossTrainPoint = loss_point(dataTrain, kMean, power, k, point_price)
    # lossTestPoint = loss_point(dataTest, kMean, power, k, point_price)
    # lossAntena = loss_antena(antena, kMean, antena_power, k, antena_price)
    # print(lossTrainPoint)
    # print(lossTestPoint)
    # print(lossAntena)
    dfTest = df_maker(dataTest, 'Test', 0)
    dfTrain = df_maker(dataTrain, 'Train', 0)
    dfAntena = df_maker(antena, 'Antena', 1)
    dfCenter1 = df_maker_center(center1, 'center1')
    dfCenter2 = df_maker_center(center2, 'center2')
    dfCenter3 = df_maker_center(center3, 'center3')

    totals = pd.concat([dfTest, dfTrain, dfAntena, dfCenter1, dfCenter2, dfCenter3], axis=0)
    # plt.scatter(X[:,0], X[:,1], color = 'b')
    # plt.scatter(antena[:,0], antena[:,1], color = 'r')
    # plt.show()
    totals.to_excel('data1000.xlsx')