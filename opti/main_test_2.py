import pandas as pd
from optimization import *


if __name__ == '__main__':
    k = 5
    step = 1
    power_iteration = 1
    kind = 'control center'
    data = pd.read_excel('data.xlsx')
    df_control_center = data[data['class'] == 'control_center'][['x', 'y', 'class']]
    df_population = data[data['class'] == 'population'][['x', 'y', 'class']]
    control_center = df_control_center[['x', 'y']].to_numpy()
    antena = data[data['class'] == 'antena'][['x', 'y']].to_numpy()
    population = df_population[['x', 'y']].to_numpy()
    df_antena = pow_iteration(population, antena, control_center, k, kind)
    df_population['power'] = 2
    df_control_center['power'] = 4
    control_price = pd.concat([df_antena, df_control_center, df_population])
    control_price.to_excel('test_2_control_price.xlsx')
    kind = 'population'
    df_antena = pow_iteration(population, antena, control_center, k, kind)
    df_population['power'] = 2
    df_control_center['power'] = 4
    control_price = pd.concat([df_antena, df_control_center, df_population])
    control_price.to_excel('test_2_population.xlsx')