import numpy as np
import pandas as pd
D =2
N = 3000
k = 2
m = 2
X1 = np.random.randn(N,D) + np.array([0, -5])
X2 = np.random.randn(N,D) + np.array([50, 100])
data = np.concatenate((X1,X2))
np.random.shuffle(data)
kMean = np.random.randn(k, D)
def train(A,means):
    n = len(A)
    B = np.zeros([n,k])
    for i in xrange(n):
        B[i,:] = w(A[i,:],means)
    Bsum = B.sum(axis=0)
    for j in xrange(k):
        u = B[:,j].reshape(n,1)
        uSum = (u*A).sum(axis=0)
        means[j,:] = uSum/Bsum[j]
    return means
def w(a,means):
    a = a.reshape(1,D)
    c = (a-means)**2
    c = c.sum(axis=1)
    c = c.reshape(1,k)
    b = np.ones([k,1])
    cMatrix =  b.dot(c)
    c = c.reshape(k,1)
    total = c/cMatrix
    total = total.sum(axis=1)
    return 1/total

for i in xrange(100):

    kMean = train(data,kMean)
print kMean

