import numpy as np
import pandas as pd


def generate_city(min_population, max_population, city_number):
    X = 10 * np.random.randn(int(np.random.uniform(min_population, max_population, 1)), 2) \
        + np.array([np.random.uniform(-80,80,1)[0], np.random.uniform(-80,80,1)[0]])
    for i in range(city_number):
        generated_point = (10 * np.random.randn(int(np.random.uniform(min_population, max_population, 1)), 2)
                      + np.array([np.random.uniform(-80,80,1)[0], np.random.uniform(-80,80,1)[0]]))
        X = np.concatenate((X,generated_point))
    df_city_population = pd.DataFrame(X, columns=['x', 'y'])
    df_city_population['class'] = 'population'
    return df_city_population


def generate_center(center_number, name):
    centers = []
    for i in range(center_number):
        centers.append([np.random.uniform(-50,50,1)[0], np.random.uniform(-50, 50,1)[0]])
    df_centers = pd.DataFrame(np.array(centers), columns=['x', 'y'])
    df_centers['class'] = name
    return df_centers


if __name__ == '__main__':
    k = 5
    k_population = 15
    control_center = generate_center(k, 'control_center')
    antena_center = generate_center(k, 'antena')
    city_population = generate_city(100, 500, k_population)
    data = pd.concat([control_center, city_population, antena_center])
    data.to_excel('data.xlsx')
