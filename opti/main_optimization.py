import numpy as np
import pandas as pd
from random import randint
import matplotlib.pyplot as plt
from numpy.linalg import inv
from optimization import *


if __name__ == '__main__':
    k = 5
    step = 1
    power_iteration = 1
    kind = 'control center'
    method = 'quazi'
    data = pd.read_excel('data.xlsx')
    df_control_center = data[data['class'] == 'control_center'][['x', 'y', 'class']]
    df_population = data[data['class'] == 'population'][['x', 'y', 'class']]
    control_center = df_control_center[['x', 'y']].to_numpy()
    antena = data[data['class'] == 'antena'][['x', 'y']].to_numpy()
    population = df_population[['x', 'y']].to_numpy()
    df = opti(population, antena, control_center, step, k, method)
    for step in [10**-4, 10 ** -5, 10 ** -6, 10 ** -7]:
        method = 'decent'
        df1 = opti(population, antena, control_center, step, k, method)
        df = pd.concat([df, df1])
    df.to_excel('optimization.xlsx')