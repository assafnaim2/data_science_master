import numpy as np
import pandas as pd
from numpy.linalg import inv


def p_norm_gradient(power, point, chosen_center, price):
    a = price * (power + 1) * (chosen_center - point) ** power
    return a


def p_norm_hessian(power, point, chosen_center, price):
    a = price * (power + 1) * (power) * (chosen_center - point) ** (power - 1)
    return a


def p_norm_loss(power, point, chosen_center, price):
    a = price * abs(chosen_center - point) ** (power + 1)
    return a


def fit(A, antena, control_center, power, antena_power, k, point_price, antena_price, method):
    n = len(A)
    Gx = np.zeros([n, k])
    Gy = np.zeros([n, k])
    Hx = np.zeros([n, k])
    Hy = np.zeros([n, k])
    for i in range(n):
        chosen_center_index = nearest(A[i, :], antena, power)
        Gx[i, chosen_center_index] = p_norm_gradient(power, A[i, 0], antena[chosen_center_index, 0], point_price)
        Gy[i, chosen_center_index] = p_norm_gradient(power, A[i, 1], antena[chosen_center_index, 1], point_price)
        Hx[i, chosen_center_index] = p_norm_hessian(power, A[i, 0], antena[chosen_center_index, 0], point_price)
        Hy[i, chosen_center_index] = p_norm_hessian(power, A[i, 1], antena[chosen_center_index, 1], point_price)
    Gx_sum = Gx.sum(axis=0)
    Gy_sum = Gy.sum(axis=0)
    Hx_sum = Hx.sum(axis=0)
    Hy_sum = Hy.sum(axis=0)
    for i in range(k):
        Gx_sum[i] = Gx_sum[i] + p_norm_gradient(antena_power, control_center[i, 0], antena[i, 0], antena_price)
        Gy_sum[i] = Gy_sum[i] + p_norm_gradient(antena_power, control_center[i, 1], antena[i, 1], antena_price)
        Hx_sum[i] = Hx_sum[i] + p_norm_hessian(antena_power, control_center[i, 0], antena[i, 0], antena_price)
        Hy_sum[i] = Hy_sum[i] + p_norm_hessian(antena_power, control_center[i, 1], antena[i, 1], antena_price)
    # Hx_avg[Hx_avg == 0] = 1
    # Hy_avg[Hy_avg == 0] = 1

    vec = []
    for i in range(k):
        if method == 'quazi':
            grad = np.array([Gx_sum[i], Gy_sum[i]])
            hess = np.matrix([[float(Hx_sum[i]), 0], [0, float(Hy_sum[i])]])
            total = np.dot(inv(hess), grad)
            vec.append(total.tolist()[0])
        elif method == 'decent':
            grad = [Gx_sum[i], Gy_sum[i]]
            vec.append(grad)
    return np.array(vec)


def loss_point(A, antena, power, k, point_price):
    n = len(A)
    B = np.zeros([n, k, 2])
    for i in range(n):
        chosen_center_index = nearest(A[i, :], antena, power)
        B[i, chosen_center_index, :] = p_norm_loss(power, A[i, :], antena[chosen_center_index, :], point_price)
    B_sum = B.sum(axis=0)
    return B_sum.sum()


def loss_antena(control_center, antena, antena_power, k, antena_price):
    sum = 0
    for i in range(k):
        sum = sum + p_norm_loss(antena_power, control_center[i], antena[i], antena_price)
    return sum.sum()


def nearest(a, means, power):
    a = a.reshape(1, 2)
    c = abs(means - a) ** (power + 1)
    sum = c.sum(axis=1)
    return sum.argmin()


def pow_iteration(data, antena, control_center, k, kind):
    antena_stamp = pd.DataFrame({'x': [], 'y': [], 'class': [], 'power': []})
    name_center = np.array(['center{number}'.format(number=i + 1) for i in range(k)]).reshape(k, 1)
    antena_price = 1
    population_price = 1
    population_power = 1
    control_power = 1
    for num in range(5):
        power_array = np.ones([k, 1]) * (num * 2 + 2)
        if kind == 'population':
            population_power = num * 2 + 1
        elif kind == 'control center':
            control_power = num * 2 + 1
        antena1 = antena
        delta = 1
        while delta > 10 ** -8:
            vec = fit(data, antena1, control_center, population_power, control_power, k, population_price, antena_price,
                      'quazi')
            vec_2 = vec ** 2
            delta = vec_2.sum()
            antena1 = antena1 - vec
        antena_stamp1 = pd.DataFrame(np.concatenate([antena1, name_center, power_array], axis=1),
                                     columns=['x', 'y', 'class', 'power'])
        antena_stamp = pd.concat([antena_stamp, antena_stamp1])
    return antena_stamp


def price_iteration(data, antena, control_center,  k, kind, method):
    antena_stamp = pd.DataFrame({'x': [], 'y': [], 'class': [], 'price': []})
    name_center = np.array(['center{number}'.format(number=i + 1) for i in range(k)]).reshape(k, 1)
    antena_price = 1
    population_price = 1
    for price in range(11):
        if kind == 'population':
            population_price = 100 * price + 1
        elif kind == 'control center':
            antena_price = 100 * price + 1
        antena1 = antena
        delta = 2
        while delta > 10 ** -8:
            vec = fit(data, antena1, control_center, 1, 1, k, population_price, antena_price, method)
            vec_2 = vec ** 2
            delta = vec_2.sum()
            antena1 = antena1 - vec
        price_array = np.ones([k, 1]) * (100 * price + 1)
        antena_stamp1 = pd.DataFrame(np.concatenate([antena1, name_center, price_array], axis=1),
                                     columns=['x', 'y', 'class', 'price'])
        antena_stamp = pd.concat([antena_stamp, antena_stamp1])
    return antena_stamp


def opti(data, antena, control_center, step, k, method):
    antena_price = 500
    population_price = 1
    antena1 = antena
    delta = 2
    number = 0
    total = []
    while delta > 10 ** -8 and number < 3000:
        vec = fit(data, antena1, control_center, 1, 1, k, population_price, antena_price, method)
        vec = vec * step
        vec_2 = vec ** 2
        delta = vec_2.sum()
        antena1 = antena1 - vec
        loss = loss_antena(data, antena1, 1, k, population_price) \
               + loss_antena(control_center, antena1, 1, k, antena_price)
        total.append([number, delta, loss])
        number += 1
    df = pd.DataFrame(np.array(total), columns=['iteration', 'delta', 'loss'])
    df['step'] = step
    df['method'] = method
    return df