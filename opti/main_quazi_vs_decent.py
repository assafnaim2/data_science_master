import numpy as np
import pandas as pd
from random import randint
import matplotlib.pyplot as plt
from numpy.linalg import inv
from optimization import *


if __name__ == '__main__':
    k = 5
    step = 1
    power_iteration = 1
    kind = 'control center'
    method = 'quazi'
    data = pd.read_excel('data.xlsx')
    df_control_center = data[data['class'] == 'control_center'][['x', 'y', 'class']]
    df_population = data[data['class'] == 'population'][['x', 'y', 'class']]
    control_center = df_control_center[['x', 'y']].to_numpy()
    antena = data[data['class'] == 'antena'][['x', 'y']].to_numpy()
    population = df_population[['x', 'y']].to_numpy()
    df_antena = price_iteration(population, antena, control_center, step, k, kind, method)
    df_population['price'] = 100
    df_control_center['price'] = 400
    control_price = pd.concat([df_antena, df_control_center, df_population])
    control_price.to_excel('test_quazi.xlsx')
    step = 10 ** -6
    method = 'decent'
    df_antena = price_iteration(population, antena, control_center, step, k, kind, method)
    df_population['price'] = 100
    df_control_center['price'] = 400
    control_price = pd.concat([df_antena, df_control_center, df_population])
    control_price.to_excel('test_decent.xlsx')