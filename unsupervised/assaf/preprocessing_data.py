import pandas as pd
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA, FastICA
import numpy as np


def min_max(data):
    scale = MinMaxScaler()
    return scale.fit_transform(data)


def z_score(data):
    scale = StandardScaler()
    return scale.fit_transform(data)


def nine_pca(data):
    pca = PCA(n_components=0.9)
    return pca.fit_transform(data)


def nine_ica(data):
    ica = FastICA(n_components=0.9)
    return ica.fit_transform(data)


def one_class_data(data, cls):
    return data[data['Class'] == cls].drop('Class', axis=1)


def concat(data, cls):
    return pd.concat([pd.DataFrame(data), cls], axis=1)


def f1_fun(precision, recall):
    return (2 * precision * recall) / (precision + recall)


def to_df(data, name):
    df = pd.DataFrame(data)
    df['value'] = name
    return df


def precision_recall(cls, array, trues):
    t = cls[array]
    tp = t.sum()
    tp_fp = t.count()
    precision = tp[0] / float(tp_fp[0])
    recall = tp[0] / float(trues)
    return precision, recall


def f1_score_p_r(array_p_all, cls, trues):
    a = concat(array_p_all, cls)
    a.columns = ['ans', 'Class']
    mean_1 = np.mean(a[a['Class'] == 1]['ans'])
    mean_0 = np.mean(a[a['Class'] == 0]['ans'])
    delta = round(mean_0 - mean_1)
    precision = 0
    recall = 0
    max = 0
    j= 0
    c = cls == 0
    for i in range(-100,100):
        x = mean_0 - (i / float(100)) * delta
        array = array_p_all < x
        p,r = precision_recall(cls, array, trues)
        f1 = f1_fun(p, r)
        if f1 > max:
            max = f1
            precision = p
            recall = r
            j = i
    x = mean_0 - (j / float(100)) * delta
    return max, precision, recall


def day_part(time):
    return np.mod(time, 60 * 60 * 24)


def pca_and_concatenate(data, time_amount, percent):
    pca = PCA(n_components=percent)
    data_min_max_pca = pca.fit_transform(data)
    if len(time_amount) != 0:
        data_time_amount = z_score(time_amount)
        data_pca = np.concatenate([data_min_max_pca, data_time_amount], axis=1)
    else:
        data_pca = data_min_max_pca
    return data_pca


def score_to_df(precision, recall, threshold, F1, percent):
    df_R = pd.DataFrame(np.concatenate([precision, recall, threshold], axis=1), columns=['precision', 'recall', 'threshold'])
    df_R['value'] = '{value}'.format(value=str(percent))
    df_F11 = pd.DataFrame(np.concatenate([F1, threshold],axis=1), columns=['F1_score', 'threshold'])
    df_F11['value'] = '{value}'.format(value=str(percent))
    return df_R, df_F11


def opti_d1(df, trues):
    mean_regular = df[df['Class'] == 0]['value'].mean()
    mean_fraud = df[df['Class'] == 1]['value'].mean()
    delta = mean_fraud - mean_regular
    F1_score = []
    pr = []
    pre = 0
    rec = 0
    max_f1 = 0
    for i in range(100):
        d_i = i / float(100)
        thresh = d_i * delta + mean_regular
        tp = df[df['value'] > thresh]['Class'].sum()
        total_anomaly_report = df[df['value'] > thresh]['Class'].count()
        recall = tp / float(trues)
        precision = tp / float(total_anomaly_report)
        pr.append([precision, recall])
        f1 = (2 * precision * recall) / (recall + precision)
        F1_score.append([f1, thresh])
        if f1 > max_f1:
            max_thresh = thresh
            max_f1 = f1
            pre = precision
            rec = recall
    PR = pd.DataFrame(pr, columns=['precision', 'recall'])
    df_f1 = pd.DataFrame(F1_score, columns= ['f1_score','treshholds'])
    df_f1.to_excel('part4.4-d1_F1.xlsx')
    PR.to_excel('part4.5-d1_PR.xlsx')
    return max_thresh, max_f1, pre, rec


def organize_data(df):
    data_min_max = min_max(df.drop(['Time', 'Amount', 'Class'], axis=1))
    pca = PCA(n_components=0.9)
    data_min_max_pca = pca.fit_transform(data_min_max)
    data_time_amount = z_score(df[['Time', 'Amount']])
    data_pca = np.concatenate([data_min_max_pca, data_time_amount], axis=1)
    data = np.concatenate([data_min_max, data_time_amount], axis=1)
    return data, data_pca