import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import preprocessing_data as prd
import multivariate_fun as mvf


if __name__ == '__main__':
    df = pd.read_csv('creditcard_no_noise.csv')
    df_noise = pd.read_csv('creditcard.csv')
    trues = len(df[df['Class'] == 1])
    trues_noise = len(df_noise[df_noise['Class'] == 1])
    PercFraud = len(df[df['Class'] == 1]) / len(df)
    df['Time'] = df.apply(lambda q: prd.day_part(q['Time']), axis=1)
    df_noise['Time'] = df_noise.apply(lambda q: prd.day_part(q['Time']), axis=1)
    cls_noise = df_noise[['Class']]
    cls = df[['Class']]
    data_min_max_noise = prd.min_max(df_noise.drop(['Time', 'Amount', 'Class'], axis=1))
    data_time_amount_noise = prd.z_score(df_noise[['Time', 'Amount']])
    data_min_max = prd.min_max(df.drop(['Time', 'Amount', 'Class'], axis=1))
    data_time_amount = prd.z_score(df[['Time', 'Amount']])

    #first check with noisy data
    mvf.total_multi(data_min_max_noise, data_time_amount_noise, cls_noise, 'noise', trues_noise, 'part1')
    #first check no noise
    mvf.total_multi(data_min_max, data_time_amount, cls, 'no_noise', trues, 'part2')
    #only v1-v28 (no noise)
    mvf.total_multi(data_min_max, np.array([]), cls, 'v28', trues, 'part3')
    mvf.merge_8_9()
    #only v1-v28 with 2 checked
    pca = PCA(n_components=0.9)
    data_9 = pca.fit_transform(data_min_max)
    p = mvf.p_multivariate_normal(data_9)
    data_check1 = data_9[p < 5.2]
    c = cls[p < 5.2].reset_index()
    c = c.drop('index', axis=1)
    pca = PCA(n_components=2)
    d2 = pca.fit_transform(data_check1)
    plt.scatter(d2[:, 0], d2[:, 1], c=c['Class'])
    plt.savefig('part4.2-scatter.png')
    plt.show()
    pca = PCA(n_components=1)
    data_check2 = pca.fit_transform(data_check1)
    df_1 = pd.concat([pd.DataFrame(data_check2, columns=['value']), c], axis=1)
    df_1.to_excel('part4.3-d1_hist.xlsx')
    treshold, f1, precision, recall = prd.opti_d1(df_1, c.sum()['Class'])
    print('2 checks --- ', 'threshold - ', round(treshold, 3), 'f1_score - ',
          round(f1, 3), ' precision - ', round(precision, 3), ' recall - ', round(recall, 3))


    # train and test
    mvf.train_test(data_9, cls)