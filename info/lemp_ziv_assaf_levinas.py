import pandas as pd
import numpy as np
a = open('levinas_right_click.txt', encoding='latin-1')
file_str = a.read()
by_words = [s for q in file_str.split("\n") for s in q.split(" ")]
freq = {}
from math import floor, ceil
from typing import AnyStr
from sys import getsizeof

ASCII_TO_INT: dict = {i.to_bytes(1, 'big'): i for i in range(256)}
INT_TO_ASCII: dict = {i: b for b, i in ASCII_TO_INT.items()}


def compress(data: AnyStr) -> bytes:
    if isinstance(data, str):
        data = data.encode()
    keys: dict = ASCII_TO_INT.copy()
    n_keys: int = 256
    compressed: list = []
    start: int = 0
    n_data: int = len(data)+1
    while True:
        if n_keys >= 512:
            keys = ASCII_TO_INT.copy()
            n_keys = 256
        for i in range(1, n_data-start):
            w: bytes = data[start:start+i]
            if w not in keys:
                compressed.append(keys[w[:-1]])
                keys[w] = n_keys
                start += i-1
                n_keys += 1
                break
        else:
            compressed.append(keys[w])
            break
    bits: str = ''.join([bin(i)[2:].zfill(9) for i in compressed])
    return int(bits, 2).to_bytes(ceil(len(bits) / 8), 'big')


def decompress(data: AnyStr) -> bytes:
    if isinstance(data, str):
        data = data.encode()
    keys: dict = INT_TO_ASCII.copy()
    bits: str = bin(int.from_bytes(data, 'big'))[2:].zfill(len(data) * 8)
    n_extended_bytes: int = floor(len(bits) / 9)
    bits: str = bits[-n_extended_bytes * 9:]
    data_list: list = [int(bits[i*9:(i+1)*9], 2)
                       for i in range(n_extended_bytes)]
    previous: bytes = keys[data_list[0]]
    uncompressed: list = [previous]
    n_keys: int = 256
    for i in data_list[1:]:
        if n_keys >= 512:
            keys = INT_TO_ASCII.copy()
            n_keys = 256
        try:
            current: bytes = keys[i]
        except KeyError:
            current = previous + previous[:1]
        uncompressed.append(current)
        keys[n_keys] = previous + current[:1]
        previous = current
        n_keys += 1
    return b''.join(uncompressed)

def compr(by_words, how_many_words_in_seq):
    return [" ".join([by_words[i+j] for j in range(how_many_words_in_seq)]) for i in range(len(by_words) - how_many_words_in_seq-1)]

vec = by_words
# for i in range(3):
#     vec = vec + compr(by_words, i+2)
df = pd.DataFrame(vec, columns = ['word'])
df['num'] = 1
dfs = df.groupby('word').count().reset_index()
dfs.columns = ['word', 'count']
dfs['profit'] = dfs.apply(lambda q: (len(q['word']) - 1)* q['count'],axis=1)
dfs = dfs.sort_values('profit', ascending=False)
array100 = []
for i in range(500):
    array100.append(chr(i))
set100 = set(array100)
set_all = set(list(file_str))
unique = list(set100.difference(set_all))
array = dfs.head(len(unique))['word'].values
j = 0
by_words = np.array(by_words)
for i in array:
    by_words[by_words == i] = unique[j]
    j += 1
by_words = by_words.tolist()
new_str = ' '.join(by_words)
f = open('after_zip1.txt', 'w')
f.write(new_str)
# print(getsizeof(compress(file_str))/1048576)
# print(getsizeof(compress(new_str))/1048576)
# dfs.sort_values('')
