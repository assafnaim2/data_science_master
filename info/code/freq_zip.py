import pandas as pd
import numpy as np
import sys


class freqzipEncoding:
    def __init__(self, text):
        self.text = text
        self.by_words = []
        self.chrs = []
        self.df_profit = pd.DataFrame([])
        self.profit_words = []
        self.profit_words_and_chr = []
        self.big_chr_switch = []

    def switch_big_chr(self):
        text_list = list(self.text)
        ord_list = np.array(list(map(ord, text_list)))
        big_ord = list(set([text_list[i] for i in range(len(text_list)) if ord_list[i] >= 255]))
        n = len(big_ord)
        arrayBig = []
        for i in range(125):
            arrayBig.append(chr(i + 125))
        setBig = set(arrayBig)
        set_all = set(text_list)
        uniqueBig = list(setBig.difference(set_all))
        uniqueBig = uniqueBig[:n]
        big_4 = []
        str_list = np.array(text_list)
        for i in range(n):
            str_list[str_list == big_ord[i]] = uniqueBig[i]
            big_4.append(str(ord(big_ord[i])) + uniqueBig[i])
        self.text = ''.join(str_list.tolist())
        self.big_chr_switch = big_4

    def df_profit_words(self):
        self.by_words = [s for s in self.text.split(" ")]
        df = pd.DataFrame(self.by_words, columns=['word'])
        df['num'] = 1
        df_profit = df.groupby('word').count().reset_index()
        df_profit.columns = ['word', 'count']
        df_profit['profit'] = df_profit.apply(lambda q: (len(q['word']) - 1) * q['count'], axis=1)
        self.df_profit = df_profit.sort_values('profit', ascending=False)

    def un_used_chr(self, n):
        array100 = []
        lens = 250
        for i in range(lens):
            array100.append(chr(i + 5))
        set100 = set(array100)
        set_all = set(list(self.text))
        unique = list(set100.difference(set_all))
        print(len(unique))
        df_final = self.df_profit.head(len(unique))
        df_final['unique'] = unique
        df_final['word_with_4'] = df_final.apply(lambda q: q['word'] + q['unique'], axis=1)
        self.profit_words = df_final['word'].values
        self.profit_words_and_chr = df_final['word_with_4'].values
        self.chrs = unique

    def change_words(self):
        j = 0
        by_words = np.array(self.by_words)
        for i in self.profit_words:
            by_words[by_words == i] = self.chrs[j]
            j += 1
        self.by_words = by_words.tolist()
        self.text = ' '.join(self.by_words)

    def compress(self, n):
        self.switch_big_chr()
        self.df_profit_words()
        self.un_used_chr(n)
        self.change_words()
        array_4 = self.profit_words_and_chr.tolist()
        big_4 = chr(4).join(self.big_chr_switch)
        array_4 = chr(4).join(array_4)
        self.text = big_4 + chr(4) + self.text + chr(4) + array_4
        print('freq compress finished')
        return self.text


class freqzipDecoding:
    def __init__(self, text):
        self.text = text
        self.by_words = []

    def switch_big_chr(self):
        by_words = [s for s in self.text.split(chr(4))]
        max = 0
        index = 0
        for i in range(len(by_words)):
            if len(by_words[i]) > max:
                max = len(by_words[i])
                index = i
        new_str = by_words[index]
        for i in by_words[:index]:
            new_str = new_str.replace(i[-1], chr(int(i[:-1])))
        self.text = new_str
        self.by_words = by_words[index+1:]

    def decompress(self):
        self.switch_big_chr()
        new_str = self.text
        for i in self.by_words:
            new_str = new_str.replace(i[-1], i[:-1])
        print('\nfreq decompress finished')
        return new_str


# if __name__ == '__main__':
#     a = open("dickens.txt", encoding='cp1252')
#     text = a.read()
#     for i in range(0,30,5):
#         freq = freqzipEncoding(text)
#         comp = freq.compress(i)
#         print(i, sys.getsizeof(comp) / 1048576)