"""
A compressing algorithm with frequent words treatment and bzip2-like ideas.
Authors: Assaf Naim, Itay Levinas

The algorithm does the following steps:
1. Replace common words with unused ASCII characters.
   Also replace characters with ASCII ordinal larger than 255, with unused ASCII ordinals smaller than or equal
   to 255.
Apply 2 and 3 on chunks of the output of 1. The larger the chunk, the better, however the RAM
limits us from having too large chunks.
2. Burrows-Wheeler transform
3. Move-To-Front transform
Then, concatenate the treated chunks and apply 4:
4. Huffman coding

Assumptions:
1. There is a small number of characters with ASCII ordinals larger than 255 in the text, and a small number of
   different characters in the text as well. Small means enough to allow the transforms in part 1 (this assumption
   is correct for the dickens.txt file and probably for most text files, but not in general).
2. The characters Start-of-text (\002) and End-of-text (\003) do not appear in the text. These characters are used
   in the Burrows-Wheeler transform.

Compression and decompression times on the dickens.txt file
(chunk size = 10^5, i7 core, 12 GB available RAM):
Compression: around 12 and a half minutes, decompression: around 11 minutes.
"""
import pandas as pd
import os
import sys
from huffman_coding import HuffmanEncoding, HuffmanDecoding
from freq_zip import freqzipDecoding, freqzipEncoding

CHUNK_SIZE = 500000


class BzipLikeCompress:
    def __init__(self, text_path):
        filename, file_extension = os.path.splitext(text_path)  # Split 'dickens.txt' to 'dickens', '.txt'
        self.output_path = filename + "_encoded.txt"  # Output file name: 'dickens_encoded.txt'
        f = open(text_path, mode='r', encoding='cp1252')
        self.file_str = f.read()
        self.chars_in_str = [chr(i) for i in range(1, 256)]
        self.compressed_file = None
        self.chunk_size = CHUNK_SIZE
        self.parts = None
        self.separate_parts = None

    def compress(self, n):
        # The main function
        freqzip = freqzipEncoding(self.file_str)
        self.file_str = freqzip.compress(n)
        print("Frequent words and strange characters replaced\n")
        self.parts = [self.file_str[i:i + self.chunk_size]
                      for i in range(0, len(self.file_str), self.chunk_size)]
        unified_parts = []

        for i, p in enumerate(self.parts):
            new_p = self._burrows_wheeler(p)
            new_p = self._move_to_front(new_p)
            unified_parts += new_p
            sys.stdout.write("\r" + "Part %d / %d complete" % (i + 1, len(self.parts)))

        print("\nNow for Huffman")
        huff = HuffmanEncoding(unified_parts)
        self.compressed_file = huff.compress(self.output_path)
        return sys.getsizeof(self.compressed_file) / 1048576

    @staticmethod
    def _burrows_wheeler(s):
        # Given a string s, apply Burrows-Wheeler transform to s.
        # That is, permute the characters in s to create a smarter (reversible) way to represent this text.
        starting_symbol = "\002"  # Start-of-text ASCII symbol, not supposed to be in the text
        ending_symbol = "\003"  # End-of-text ASCII symbol, not supposed to be in the text
        if starting_symbol in s or ending_symbol in s:
            raise TypeError("The string should not contain Start-of-text or End-of-text characters")
        s = starting_symbol + s + ending_symbol
        length = len(s)
        s_cyc = s * 2  # double the string to not mess with cyclic issues and modulus.

        class K:  # The class for each "row" in the "table".
            def __init__(self, i):
                self.i = i

            # The function that defines whether our row is before the row starting with b.
            # Used for sorting.
            def __lt__(self, b):
                i, j = self.i, b.i
                for k in range(length):
                    if s_cyc[i + k] < s_cyc[j + k]:
                        return True
                    elif s_cyc[i + k] > s_cyc[j + k]:
                        return False
                return False  # they're equal

        # Sort the "rows" in the "table" by the key we defined above.
        inorder = sorted(range(length), key=K)
        return "".join(s_cyc[i + length - 1] for i in inorder)

    def _move_to_front(self, s):
        # Given a string s and knowing the characters s may have, apply the move-to-front transform on s.
        # That is, encode each of its characters by an index, in the (reversible) way of the algorithm.

        # We iteratively get the index of each character in the existing alphabet self.chars_in_str,
        # then replace the first character and the character we just replaced in the alphabet, before
        # continuing to the next letter:
        return [[self.chars_in_str.index(ch),
                 self.chars_in_str.insert(0, self.chars_in_str.pop(self.chars_in_str.index(ch)))][0]
                for ch in s]

    @property
    def plain_file_size(self):
        # Size of plain text in megabytes
        return sys.getsizeof(self.file_str) / 1048576

    @property
    def comp_file_size(self):
        # Size of compressed text in megabytes
        return sys.getsizeof(self.compressed_file) / 1048576


class BzipLikeDecompress:
    def __init__(self, compressed_text_path):
        # Split 'dickens_encoded.txt' to 'dickens_encoded', '.txt'
        filename, file_extension = os.path.splitext(compressed_text_path)
        self.output_path = filename + "_decoded.txt"  # Output file name: 'dickens_encoded_decoded.txt'
        f = open(compressed_text_path, mode='rb')
        self.compressed_file = f.read()
        self.chars_in_str = [chr(i) for i in range(1, 256)]
        self.decompressed_file = None
        self.chunk_size = CHUNK_SIZE
        self.parts = None

    def decompress(self):
        # The main function
        huff = HuffmanDecoding(self.compressed_file)
        unified_parts = huff.decompress()
        # Split into chunks like in the compression. Note that the new chunk size must be
        # the original chunk size + 2, since we added Start-of-text and End-of-text for every chunk.
        self.parts = [unified_parts[i:i + self.chunk_size + 2]
                      for i in range(0, len(unified_parts), self.chunk_size + 2)]
        self.decompressed_file = ""
        for i, p in enumerate(self.parts):
            new_p = self._inverse_move_to_front(p)
            new_p = self._inverse_burrows_wheeler(new_p)
            self.decompressed_file += new_p
            sys.stdout.write("\r" + "Part %d / %d complete" % (i + 1, len(self.parts)))
        freqzip = freqzipDecoding(self.decompressed_file)
        self.decompressed_file = freqzip.decompress()
        print("\nDecompressed")
        with open(self.output_path, 'w', encoding='cp1252') as out:  # , encoding="latin-1") as out:
            out.write(self.decompressed_file)

    @staticmethod
    def _inverse_burrows_wheeler(r):
        # Given a string r, apply the inverse Burrows-Wheeler transform to r.
        # That is, apply the inverse permutation to receive the original text from r.
        # No table in the implementation, it is unnecessary.

        # The row in the table (not built but exists) of the original string - wherever the string ends with
        # the End-of-text character:
        the_correct_row = r.index("\003")

        def row(k):
            permutation = sorted((t, i) for i, t in enumerate(r))
            for _ in r:
                t, k = permutation[k]
                yield t

        s = "".join(row(the_correct_row))
        return s.rstrip("\003").strip("\002")  # Get rid of start and end markers

    def _inverse_move_to_front(self, r):
        # Given a string r and knowing the characters s may have, apply the inverse move-to-front transform on r.
        # That is, decode the encoded representation which r is.
        return ''.join([self.chars_in_str[i], self.chars_in_str.insert(0, self.chars_in_str.pop(i))][0] for i in r)

    @property
    def comp_file_size(self):
        # Size of compressed text in megabytes
        return sys.getsizeof(self.compressed_file) / 1048576

    @property
    def decomp_file_size(self):
        # Size of decompressed text in megabytes
        return sys.getsizeof(self.decompressed_file) / 1048576


if __name__ == '__main__':
    vec = []


    enc = BzipLikeCompress(os.path.join(os.getcwd(), "dickens.txt"))
    i = 250
    a = enc.compress(i)

    print([i, a])
    #pd.DataFrame(vec).to_excel('find_min.xlsx')
    # dec = BzipLikeDecompress(os.path.join(os.getcwd(), "dickens_encoded.txt"))
    # dec.decompress()
